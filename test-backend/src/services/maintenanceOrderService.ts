export class MaintenanceOrderService {
    private _firestoreService: any = null;
    private static instance: MaintenanceOrderService;
    private constructor(firestoreService) {
        this._firestoreService = firestoreService;
    }

    public static getInstance(firestoreService): MaintenanceOrderService {
        if (!MaintenanceOrderService.instance) {
            MaintenanceOrderService.instance = new MaintenanceOrderService(firestoreService);
        }
        return MaintenanceOrderService.instance;
    }
}
