export class FirestoreService {
    public adminRef: any = null;
    private db: any = null;
    private static instance: FirestoreService;
    private constructor(admin) {
        this.adminRef = admin;
        this.db = admin.firestore();
    }

    public static getInstance(admin: any): FirestoreService {
        if (!FirestoreService.instance) {
            FirestoreService.instance = new FirestoreService(admin);
        }
        return FirestoreService.instance;
    }

    public UpdateBatch(updateData){
        var batch = this.db.batch();

        for(var k in updateData){
            console.log(k);
            var res = k.split("/");
            console.log(res);
            
            var nycRef = this.db.collection(res[0]).doc(res[1]);
            batch.set(nycRef, updateData[k]);
        }


        // Update the population of 'SF'
       // var sfRef = this.db.collection("cities").doc("SF");
        //batch.update(sfRef, {"population": 1000000});

        // Delete the city 'LA'
        //var laRef = this.db.collection("cities").doc("LA");
        //batch.delete(laRef);

        // Commit the batch
        batch.commit().then(() => {
            // ...
            console.log("Actializado");
            
        }).catch((err)=>{
            console.log(err);
            
        });
    }
}
