export class UserService {
    private static instance: UserService;
    private _firestoreService: any = null;
    private constructor(firestoreService) {
        this._firestoreService = firestoreService;
    }

    public static getInstance(firestoreService): UserService {
        if (!UserService.instance) {
            UserService.instance = new UserService(firestoreService);
        }
        return UserService.instance;
    }

    public UpdateUser(){
        const updateData = {
            'users/test':{
                'name': 'Arbey',
                'status': 0,
                'phone':'+57 3118352830',
            }
        }

        this._firestoreService.UpdateBatch(updateData);
    }
}
