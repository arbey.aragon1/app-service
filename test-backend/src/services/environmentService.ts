const dotenv = require("dotenv");
dotenv.config();
export class EnvironmentService {
    private static instance: EnvironmentService;
    public readonly ENVIRONMENT: string;
    public readonly CREDANTEIAL_PATH: string;

    private constructor() {
        this.ENVIRONMENT = process.env.ENVIRONMENT;
        this.CREDANTEIAL_PATH = process.env.CREDANTEIAL_PATH;
        console.log(this.ENVIRONMENT);
        console.log(this.CREDANTEIAL_PATH);
    }

    public static getInstance(): EnvironmentService {
        if (!EnvironmentService.instance) {
            EnvironmentService.instance = new EnvironmentService();
        }
        return EnvironmentService.instance;
    }
}
