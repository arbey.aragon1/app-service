import { Observable, from, of } from "rxjs";
import { tap, map, switchMap } from "rxjs/operators";
import * as admin from "firebase-admin";
var fs = require("fs");

export class FirebaseService {
    private static instance: FirebaseService;
    public adminRef: any = null;
    private _dbReference: any = null;
    private constructor() {
        var obj = JSON.parse(
            fs.readFileSync(
                "C:\\Users\\arbey\\Documents\\gcp-credential.json",
                "utf8"
            )
        );
        admin.initializeApp({
            credential: admin.credential.cert({
                projectId: obj.project_id,
                privateKey: obj.private_key?.replace(/\\n/g, "\n"),
                clientEmail: obj.client_email,
            }),
            databaseURL: "https://app-services-1d27e.firebaseio.com",
        });
        this._dbReference = admin.database();
        this.adminRef = admin;
    }

    public static getInstance(): FirebaseService {
        if (!FirebaseService.instance) {
            FirebaseService.instance = new FirebaseService();
        }
        return FirebaseService.instance;
    }

    public FetchFromPath(path: string): Observable<any> {
        return new Observable((observer) => {
            this._dbReference.ref(path).on(
                "value",
                (snapshot) => {
                    observer.next({ key: snapshot.key, ...snapshot.val() });
                    // observer.complete();
                },
                (err) => {
                    observer.error(err);
                }
            );
        });
    }

    public FetchFromPathFirtered(path: string): Observable<any> {
        return new Observable((observer) => {
            this._dbReference
                .ref(path)
                .orderByChild("msSelected")
                .limitToLast(1)
                .on(
                    "value",
                    (snapshot) => {
                        if (snapshot.val() != null) {
                            console.log(snapshot.val());
                            const obj = snapshot.val();
                            const key = Object.keys(obj)[0];

                            if (obj[key].msSelected == 2) {
                                observer.next({ key: key, value: obj[key] });
                            }
                            // observer.complete();
                        }
                    },
                    (err) => {
                        observer.error(err);
                    }
                );
        });
    }

    public FetchOnceFromPath(path: string): Observable<any> {
        return new Observable((observer) => {
            this._dbReference.ref(path).once(
                "value",
                (snapshot) => {
                    observer.next({ key: snapshot.key, ...snapshot.val() });
                    observer.complete();
                },
                (err) => {
                    observer.error(err);
                }
            );
        });
    }

    public TokenDecode(token: string): Observable<any> {
        return from(admin.auth().verifyIdToken(token));
    }

    public TokenVerification(token: string, uid: string): Observable<any> {
        return this.TokenDecode(token).pipe(
            tap((decodedToken) => {
                return {
                    uid: decodedToken.uid,
                    valid: uid == decodedToken.uid,
                };
            })
        );
    }

    public FetchWithTransaction(path: string): Observable<any> {
        var updated = false;
        console.log(path);

        return from(
            this._dbReference.ref(path + "/msSelected").transaction((value) => {
                console.log(value);
                if (value == 2) {
                    // the counter doesn't exist yet, start at one
                    updated = true;
                    return 1;
                } else {
                    // increment - the normal case
                    updated = false;
                    return;
                }
            })
        ).pipe(
            switchMap(() => {
                if (updated) {
                    return this.FetchOnceFromPath(path).pipe(
                        map((v) => ({ updated: updated, value: v }))
                    );
                } else {
                    return of({ updated: updated, value: {} });
                }
            })
        );
    }

    public WriteBatch() {
        const batch = this._dbReference.batch();
    }

    public Delete(path: string): Observable<any> {
        return from(this._dbReference.ref(path).remove());
    }
}
