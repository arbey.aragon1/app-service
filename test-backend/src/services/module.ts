import { FirebaseService } from "./firebaseService";
import { EnvironmentService } from "./environmentService";
import { FirestoreService } from "./firestoreService";
import { MaintenanceOrderService } from "./maintenanceOrderService";
import { UserService } from "./userService";

export class Module {
    private static instance: Module;
    private _firebaseService: FirebaseService;
    private _environmentService: EnvironmentService;
    private _firestoreService: FirestoreService;
    private _maintenanceOrderService: MaintenanceOrderService;
    private _userService: UserService;

    private constructor() {
        this._environmentService = EnvironmentService.getInstance();
        this._firebaseService = FirebaseService.getInstance();
        this._firestoreService = FirestoreService.getInstance(this._firebaseService.adminRef);
        this._userService = UserService.getInstance(this._firestoreService);
        this._maintenanceOrderService = MaintenanceOrderService.getInstance(this._firestoreService);
    }

    public static getInstance(): Module {
        if (!Module.instance) {
            Module.instance = new Module();
        }
        return Module.instance;
    }

    public FirebaseService(): FirebaseService {
        return this._firebaseService;
    }

    public FirestoreService(): FirestoreService {
        return this._firestoreService;
    }

    public EnvironmentService(): EnvironmentService {
        return this._environmentService;
    }

    public MaintenanceOrderService(): MaintenanceOrderService {
        return this._maintenanceOrderService;
    }

    public UserService(): UserService {
        return this._userService;
    }
}
