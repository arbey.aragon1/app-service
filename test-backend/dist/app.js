"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
const module_1 = require("./services/module");
const moduleInst = module_1.Module.getInstance();
const firebaseService = moduleInst.FirebaseService();
const environmentService = moduleInst.EnvironmentService();
const userService = moduleInst.UserService();
userService.UpdateUser();
/*
// Get a new write batch
var batch = db.batch();

// Set the value of 'NYC'
var nycRef = db.collection("cities").doc("NYC");
batch.set(nycRef, {name: "New York City"});

// Update the population of 'SF'
var sfRef = db.collection("cities").doc("SF");
batch.update(sfRef, {"population": 1000000});

// Delete the city 'LA'
var laRef = db.collection("cities").doc("LA");
batch.delete(laRef);

// Commit the batch
batch.commit().then(() => {
    // ...
});/** */
/*
of({})
    .pipe(
        switchMap(() => {
            return firebaseService.FetchFromPathFirtered("/usersMS");
        }),
        map((val) => {
            //manipulacion de los datos
            const value: MsgMs = {
                uidSender: val.value.uid,
                token: val.value.token,
                keyMsg: val.key,
                msg: {},
                commited: false,
                validToken: false,
                uidInToken: "",
            };
            return value;
        }),
        switchMap((value) => {
            var path = "/usersMS/" + value.keyMsg;
            return firebaseService.FetchWithTransaction(path).pipe(
                map((v) => {
                    value.commited = v.updated;
                    value.msg = v.value;
                    return value;
                })
            );
        }),
        switchMap((value) => {
            //Valida token
            return firebaseService
                .TokenVerification(value.token, value.uidSender)
                .pipe(
                    map((v) => {
                        value.validToken = v.valid;
                        value.uidInToken = v.uid;
                        return value;
                    })
                );
        }),
        switchMap((value) => {
            var path = "/usersMS/" + value.keyMsg;
            return firebaseService.Delete(path).pipe(
                map(() => {
                    return value;
                })
            );
        }),
        switchMap(() => {
            //Actualizaciones de datos
            return of({});
        }),
        switchMap(() => {
            //Envia respuesta
            return of({});
        })
    )
    .subscribe(
        (val) => {
            console.log(val);
        },
        (err) => {
            console.log("Error");
            console.log(err);
        }
    );

    /** */
/*
var firebaseConfig = {
    apiKey: "AIzaSyCGPnOdnDFZmnzVC9eIeNShIXSzZZD5lX0",
    authDomain: "app-services-1d27e.firebaseapp.com",
    databaseURL: "https://app-services-1d27e.firebaseio.com",
    projectId: "app-services-1d27e",
    storageBucket: "app-services-1d27e.appspot.com",
    messagingSenderId: "497033520090",
    appId: "1:497033520090:web:499f9d71b21e53450ad6e3",
    measurementId: "G-C9QR4YK5L5",
};

firebase.initializeApp(firebaseConfig);

var database = firebase.database();
console.log("Hello");

const nameRef = database.ref().child("object").child("name");
nameRef.on("value", (snapshot) => {
    console.log(snapshot.val());
});/** */
/*
var obj = JSON.parse(
    fs.readFileSync("C:\\Users\\arbey\\Documents\\gcp-credential.json", "utf8")
);

admin.initializeApp({
    credential: admin.credential.cert({
        projectId: obj.project_id,
        privateKey: obj.private_key?.replace(/\\n/g, "\n"),
        clientEmail: obj.client_email,
    }),
    databaseURL: "https://app-services-1d27e.firebaseio.com",
});

var db = admin.database();
var ref = db.ref("/usersMS/ZO68Bpkf3gZ7BIRTMpHf0SWhFWl2");
ref.on("value", function (snapshot) {
    console.log(snapshot.val());
    const values = snapshot.val()
    admin
        .auth()
        .verifyIdToken(values.token)
        .then((decodedToken) => {
            const uid = decodedToken.uid;
        })
        .catch((error) => {});
});
/** */
//# sourceMappingURL=app.js.map