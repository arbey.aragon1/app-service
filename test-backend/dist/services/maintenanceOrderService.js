"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MaintenanceOrderService = void 0;
class MaintenanceOrderService {
    constructor() { }
    static getInstance() {
        if (!MaintenanceOrderService.instance) {
            MaintenanceOrderService.instance = new MaintenanceOrderService();
        }
        return MaintenanceOrderService.instance;
    }
}
exports.MaintenanceOrderService = MaintenanceOrderService;
//# sourceMappingURL=maintenanceOrderService.js.map