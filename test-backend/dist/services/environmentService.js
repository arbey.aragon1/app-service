"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnvironmentService = void 0;
const dotenv = require("dotenv");
dotenv.config();
class EnvironmentService {
    constructor() {
        this.ENVIRONMENT = process.env.ENVIRONMENT;
        this.CREDANTEIAL_PATH = process.env.CREDANTEIAL_PATH;
        console.log(this.ENVIRONMENT);
        console.log(this.CREDANTEIAL_PATH);
    }
    static getInstance() {
        if (!EnvironmentService.instance) {
            EnvironmentService.instance = new EnvironmentService();
        }
        return EnvironmentService.instance;
    }
}
exports.EnvironmentService = EnvironmentService;
//# sourceMappingURL=environmentService.js.map