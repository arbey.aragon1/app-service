"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Module = void 0;
const firebaseService_1 = require("./firebaseService");
const environmentService_1 = require("./environmentService");
const firestoreService_1 = require("./firestoreService");
const maintenanceOrderService_1 = require("./maintenanceOrderService");
const userService_1 = require("./userService");
class Module {
    constructor() {
        this._environmentService = environmentService_1.EnvironmentService.getInstance();
        this._firebaseService = firebaseService_1.FirebaseService.getInstance();
        this._firestoreService = firestoreService_1.FirestoreService.getInstance(this._firebaseService.adminRef);
        this._maintenanceOrderService = maintenanceOrderService_1.MaintenanceOrderService.getInstance();
        this._userService = userService_1.UserService.getInstance(this._firestoreService);
    }
    static getInstance() {
        if (!Module.instance) {
            Module.instance = new Module();
        }
        return Module.instance;
    }
    FirebaseService() {
        return this._firebaseService;
    }
    FirestoreService() {
        return this._firestoreService;
    }
    EnvironmentService() {
        return this._environmentService;
    }
    MaintenanceOrderService() {
        return this._maintenanceOrderService;
    }
    UserService() {
        return this._userService;
    }
}
exports.Module = Module;
//# sourceMappingURL=module.js.map