"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FirestoreService = void 0;
class FirestoreService {
    constructor(admin) {
        this.adminRef = null;
        this.db = null;
        this.adminRef = admin;
        this.db = admin.firestore();
    }
    static getInstance(admin) {
        if (!FirestoreService.instance) {
            FirestoreService.instance = new FirestoreService(admin);
        }
        return FirestoreService.instance;
    }
    UpdateBatch(updateData) {
        var batch = this.db.batch();
        for (var k in updateData) {
            console.log(k);
            var res = k.split("/");
            console.log(res);
            var nycRef = this.db.collection(res[0]).doc(res[1]);
            batch.set(nycRef, updateData[k]);
        }
        // Update the population of 'SF'
        // var sfRef = this.db.collection("cities").doc("SF");
        //batch.update(sfRef, {"population": 1000000});
        // Delete the city 'LA'
        //var laRef = this.db.collection("cities").doc("LA");
        //batch.delete(laRef);
        // Commit the batch
        batch.commit().then(() => {
            // ...
            console.log("Actializado");
        }).catch((err) => {
            console.log(err);
        });
    }
}
exports.FirestoreService = FirestoreService;
//# sourceMappingURL=firestoreService.js.map