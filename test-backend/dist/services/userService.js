"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
class UserService {
    constructor(firestoreService) {
        this._firestoreService = null;
        this._firestoreService = firestoreService;
    }
    static getInstance(firestoreService) {
        if (!UserService.instance) {
            UserService.instance = new UserService(firestoreService);
        }
        return UserService.instance;
    }
    UpdateUser() {
        const updateData = {
            'users/test': {
                'name': 'Arbey',
                'status': 0,
                'phone': '+57 3118352830',
            }
        };
        this._firestoreService.UpdateBatch(updateData);
    }
}
exports.UserService = UserService;
//# sourceMappingURL=userService.js.map