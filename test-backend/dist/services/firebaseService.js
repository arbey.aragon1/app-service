"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FirebaseService = void 0;
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const admin = __importStar(require("firebase-admin"));
var fs = require("fs");
class FirebaseService {
    constructor() {
        var _a;
        this.adminRef = null;
        this._dbReference = null;
        var obj = JSON.parse(fs.readFileSync("C:\\Users\\arbey\\Documents\\gcp-credential.json", "utf8"));
        admin.initializeApp({
            credential: admin.credential.cert({
                projectId: obj.project_id,
                privateKey: (_a = obj.private_key) === null || _a === void 0 ? void 0 : _a.replace(/\\n/g, "\n"),
                clientEmail: obj.client_email,
            }),
            databaseURL: "https://app-services-1d27e.firebaseio.com",
        });
        this._dbReference = admin.database();
        this.adminRef = admin;
    }
    static getInstance() {
        if (!FirebaseService.instance) {
            FirebaseService.instance = new FirebaseService();
        }
        return FirebaseService.instance;
    }
    FetchFromPath(path) {
        return new rxjs_1.Observable((observer) => {
            this._dbReference.ref(path).on("value", (snapshot) => {
                observer.next(Object.assign({ key: snapshot.key }, snapshot.val()));
                // observer.complete();
            }, (err) => {
                observer.error(err);
            });
        });
    }
    FetchFromPathFirtered(path) {
        return new rxjs_1.Observable((observer) => {
            this._dbReference
                .ref(path)
                .orderByChild("msSelected")
                .limitToLast(1)
                .on("value", (snapshot) => {
                if (snapshot.val() != null) {
                    console.log(snapshot.val());
                    const obj = snapshot.val();
                    const key = Object.keys(obj)[0];
                    if (obj[key].msSelected == 2) {
                        observer.next({ key: key, value: obj[key] });
                    }
                    // observer.complete();
                }
            }, (err) => {
                observer.error(err);
            });
        });
    }
    FetchOnceFromPath(path) {
        return new rxjs_1.Observable((observer) => {
            this._dbReference.ref(path).once("value", (snapshot) => {
                observer.next(Object.assign({ key: snapshot.key }, snapshot.val()));
                observer.complete();
            }, (err) => {
                observer.error(err);
            });
        });
    }
    TokenDecode(token) {
        return rxjs_1.from(admin.auth().verifyIdToken(token));
    }
    TokenVerification(token, uid) {
        return this.TokenDecode(token).pipe(operators_1.tap((decodedToken) => {
            return {
                uid: decodedToken.uid,
                valid: uid == decodedToken.uid,
            };
        }));
    }
    FetchWithTransaction(path) {
        var updated = false;
        console.log(path);
        return rxjs_1.from(this._dbReference.ref(path + "/msSelected").transaction((value) => {
            console.log(value);
            if (value == 2) {
                // the counter doesn't exist yet, start at one
                updated = true;
                return 1;
            }
            else {
                // increment - the normal case
                updated = false;
                return;
            }
        })).pipe(operators_1.switchMap(() => {
            if (updated) {
                return this.FetchOnceFromPath(path).pipe(operators_1.map((v) => ({ updated: updated, value: v })));
            }
            else {
                return rxjs_1.of({ updated: updated, value: {} });
            }
        }));
    }
    WriteBatch() {
        const batch = this._dbReference.batch();
    }
    Delete(path) {
        return rxjs_1.from(this._dbReference.ref(path).remove());
    }
}
exports.FirebaseService = FirebaseService;
//# sourceMappingURL=firebaseService.js.map