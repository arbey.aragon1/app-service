"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MaintenanceOrder = void 0;
class MaintenanceOrder {
    constructor(_key, _status, _description) {
        this._key = _key;
        this._status = _status;
        this._description = _description;
    }
    get key() {
        return this._key;
    }
    get status() {
        return this._status;
    }
    get description() {
        return this._description;
    }
    static fromJSON({ key, status, description, }) {
        return new MaintenanceOrder(key, status, description);
    }
    static fromJSONArray(array) {
        return array
            .map((value) => {
            return Object.assign({ uid: value.key }, value.payload.val());
        })
            .map(MaintenanceOrder.fromJSON);
    }
    /**
     * README:
     * toJSON no utiliza el key del usuario
     * Se debe utilizar este método para guardar el usuario en
     * firebase con el método push
     */
    toJSON() {
        return {
            status: this.status,
            description: this.description,
        };
    }
}
exports.MaintenanceOrder = MaintenanceOrder;
//# sourceMappingURL=maintenance-order.js.map