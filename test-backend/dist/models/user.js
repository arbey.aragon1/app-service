"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
class User {
    constructor(_uid, _status, _name, _lastName, _email, _phone) {
        this._uid = _uid;
        this._status = _status;
        this._name = _name;
        this._lastName = _lastName;
        this._email = _email;
        this._phone = _phone;
    }
    get uid() {
        return this._uid;
    }
    get status() {
        return this._status;
    }
    get name() {
        return this._name;
    }
    get lastName() {
        return this._lastName;
    }
    get email() {
        return this._email;
    }
    get phone() {
        return this._phone;
    }
    static fromJSON({ uid, status, name, lastName, email, phone, }) {
        return new User(uid, status, name, lastName, email, phone);
    }
    static fromJSONArray(array) {
        return array
            .map((value) => {
            return Object.assign({ uid: value.key }, value.payload.val());
        })
            .map(User.fromJSON);
    }
    /**
     * README:
     * toJSON no utiliza el key del usuario
     * Se debe utilizar este método para guardar el usuario en
     * firebase con el método push
     */
    toJSON() {
        return {
            status: this.status,
            name: this.name,
            lastName: this.lastName,
            email: this.email,
            phone: this.phone,
        };
    }
}
exports.User = User;
//# sourceMappingURL=user.js.map