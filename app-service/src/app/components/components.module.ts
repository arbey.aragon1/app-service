import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { MainNavComponent } from "../main-nav/main-nav.component";
import { HeaderComponent } from "../components/header/header.component";

@NgModule({
    imports: [IonicModule],
    declarations: [MainNavComponent, HeaderComponent],
    providers: [],
    exports: [MainNavComponent, HeaderComponent],
})
export class ComponentsModule {}
