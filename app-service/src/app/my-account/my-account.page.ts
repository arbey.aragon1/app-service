import { Component, OnInit } from "@angular/core";
import { NavController } from "@ionic/angular";
import { User } from "../models/users";
@Component({
    selector: "app-my-account",
    templateUrl: "./my-account.page.html",
    styleUrls: ["./my-account.page.scss"],
})
export class MyAccountPage implements OnInit {
    private user: User = null;
    constructor(private navCtrl: NavController) {}

    ngOnInit() {
        var user: User = new User(
            "uid",
            5,
            "pepito",
            "Arias",
            "pepito@gmial.com",
            "01234567"
        );
        this.user = user;
    }

    goToBack() {
        this.navCtrl.back();
    }
}
