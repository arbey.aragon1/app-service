import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VerificationSmsPage } from './verification-sms.page';

const routes: Routes = [
  {
    path: '',
    component: VerificationSmsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VerificationSmsPageRoutingModule {}
