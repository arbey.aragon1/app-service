import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VerificationSmsPageRoutingModule } from './verification-sms-routing.module';

import { VerificationSmsPage } from './verification-sms.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VerificationSmsPageRoutingModule
  ],
  declarations: [VerificationSmsPage]
})
export class VerificationSmsPageModule {}
