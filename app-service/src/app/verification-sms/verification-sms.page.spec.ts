import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VerificationSmsPage } from './verification-sms.page';

describe('VerificationSmsPage', () => {
  let component: VerificationSmsPage;
  let fixture: ComponentFixture<VerificationSmsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificationSmsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VerificationSmsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
