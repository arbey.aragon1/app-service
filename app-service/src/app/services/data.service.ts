import { Injectable } from '@angular/core';

 
 @Injectable({
   providedIn: 'root'
 })
 
 export class DataService {
 
   constructor() { }
 
   terms_of_use = 'Términos y condiciones';
   privacy_policy = 'Políticas de privacidad'
 
 }
 