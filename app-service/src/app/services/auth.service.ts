import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { BehaviorSubject, from, Observable, of, Subscription } from "rxjs";
import { tap, map, switchMap } from "rxjs/operators";
import { User } from "../models/users";
import firebase from "firebase/app";
import { UserMService } from "../services/user-m.service";

@Injectable({
    providedIn: "root",
})
export class AuthService {
    private _isLoggedIn: boolean = false;
    private _user: BehaviorSubject<User> = new BehaviorSubject<User>(undefined);
    private _userServiceSub: Subscription;

    get isLoggedIn(): boolean {
        return this._isLoggedIn;
    }

    get user(): User {
        return this._user.value;
    }

    constructor(
        private auth: AngularFireAuth,
        private userMService: UserMService
    ) {}

    public signInWithGoogle(): Observable<any> {
        return from(
            this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
        ); /*.pipe(
            switchMap((data: firebase.auth.UserCredential) => {
                return this.userService.fetchEmail(data.user.email).pipe(
                    switchMap((exist: boolean) => {
                        const user: User = new User(
                            null,
                            0,
                            data.user.displayName,
                            null,
                            data.user.email,
                            null
                        );
                        if (exist) {
                            return this.userService
                                .fetchUserByUid(data.user.uid)
                                .pipe(
                                    switchMap((userData: User) => {
                                        if (userData.status !== undefined) {
                                            return of(userData);
                                        }
                                        return this.userService
                                            .addUserForFirstTime(
                                                data.user.uid,
                                                data.user.email,
                                                user
                                            )
                                            .pipe(map(() => user));
                                    })
                                );
                        } else {
                            return this.userService
                                .addUserForFirstTime(
                                    data.user.uid,
                                    data.user.email,
                                    user
                                )
                                .pipe(map(() => user));
                        }
                    }),
                    tap(() => this.postLogin(data.user.uid))
                );
            })
        );/** */
    }

    public signInWithEmailAndPassword(
        name,
        lastName,
        phone,
        email: string,
        password: string
    ): Observable<any> {
        return of({}).pipe(
            switchMap((data) =>
                from(
                    this.auth.createUserWithEmailAndPassword(email, password)
                ).pipe(
                    map((userData: firebase.auth.UserCredential) => {
                        return {
                            ...data,
                            ...{
                                uid: userData.user.uid,
                                email: email,
                                name: name,
                                phone: phone,
                            },
                        };
                    })
                )
            ),
            switchMap((data) => {
                return from(
                    Observable.create((obs) => {
                        return firebase.auth().onAuthStateChanged(
                            (user) => {
                                if (user) {
                                    user.getIdToken().then(function (idToken) {
                                        obs.next(idToken);
                                    });
                                } else {
                                    obs.error("Erro user: ", user);
                                }
                            },
                            (err) => obs.error(err),
                            () => obs.complete()
                        );
                    })
                ).pipe(
                    map((accessToken: string) => {
                        return {
                            ...data,
                            ...{ token: accessToken, msSelected: 2 },
                        };
                    })
                );
            }),
            tap((data) => {
                return this.userMService.updateUser(data.uid, data);
            }),
            tap((idToken: any) => this.postLogin(""))
        );
    }

    public sendPasswordResetEmail(email: string): Observable<any> {
        return from(this.auth.sendPasswordResetEmail(email));
    }

    postLogin(uid: string) {
        if (!!this._userServiceSub) {
            this._userServiceSub.unsubscribe();
        }
        /*this._userServiceSub = this.userService.fetchUserByUid(uid).subscribe(
            (user: User) => {
                this._user.next(user);
            },
            (error) => {
                console.log(error);
                this.logout();
            }
        );/** */
    }

    logout(): Observable<any> {
        return from(this.auth.signOut()).pipe(tap(() => location.reload()));
    }

    public getUserObservable(): Observable<User> {
        return this._user.asObservable();
    }
}
