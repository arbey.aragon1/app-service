import { Injectable } from "@angular/core";
import { FirebaseApp } from "@angular/fire";
import { AngularFireDatabase } from "@angular/fire/database";

@Injectable({
    providedIn: "root",
})
export class UserMService {
    private key: string = "/usersMS";
    constructor(private db: AngularFireDatabase) {}

    updateUser(uid, data): any {
        return this.db.list(this.key).update(uid, data);
    }
}
