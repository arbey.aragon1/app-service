import { TestBed } from '@angular/core/testing';

import { UserMService } from './user-m.service';

describe('UserMService', () => {
  let service: UserMService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserMService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
