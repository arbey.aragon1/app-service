import { Component, OnInit } from "@angular/core";
import {
    FormGroup,
    FormBuilder,
    FormControl,
    Validators,
} from "@angular/forms";
import { NavController } from "@ionic/angular";

@Component({
    selector: "app-reset-password",
    templateUrl: "./reset-password.page.html",
    styleUrls: ["./reset-password.page.scss"],
})
export class ResetPasswordPage implements OnInit {

  resetPasswordForm: FormGroup;

    validation_message = {
        email: [
            { type: "required", message: "El email es requerido" },
            { type: "pattern", message: "Este no es un email válido" },
        ],

    };


    errorMessage = "";
    constructor( private formBuilder: FormBuilder, private navCtrl: NavController) {}

    ngOnInit() {
      this.resetPasswordForm = this.formBuilder.group({
          email: new FormControl(
              "",
              Validators.compose([
                  Validators.required,
                  Validators.pattern(
                      "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"
                  ),
              ])
          ),
      });
  }


    resetPasswordUser(credentials) {
        this.navCtrl.navigateForward("/login");
      
    }
    goToBack() {
        this.navCtrl.back();
    }
    resetPassword() {
      this.navCtrl.navigateForward("/login");
  }
}
