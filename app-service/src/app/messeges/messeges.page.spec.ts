import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MessegesPage } from './messeges.page';

describe('MessegesPage', () => {
  let component: MessegesPage;
  let fixture: ComponentFixture<MessegesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessegesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MessegesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
