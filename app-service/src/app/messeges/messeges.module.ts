import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MessegesPageRoutingModule } from './messeges-routing.module';

import { MessegesPage } from './messeges.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MessegesPageRoutingModule
  ],
  declarations: [MessegesPage]
})
export class MessegesPageModule {}
