import {FormGroup,FormBuilder, FormControl,Validators} from "@angular/forms";
import { NavController } from "@ionic/angular";
import { InfoPage } from '../info/info.page';
import { ModalController} from '@ionic/angular';
import { DataService } from '../services/data.service';
import { SwUpdate } from "@angular/service-worker";
import { Component, HostListener, OnInit } from "@angular/core";

@Component({
    selector: "app-login",
    templateUrl: "./login.page.html",
    styleUrls: ["./login.page.scss"],
})
export class LoginPage implements OnInit {
    loginForm: FormGroup;

    validation_message = {
        email: [
            { type: "required", message: "El email es requerido" },
            { type: "pattern", message: "Este no es un email válido" },
        ],
        password: [
            { type: "required", message: "El password es requerido" },
            { type: "minlength", message: "Mínimo 6 letras para el password" },
        ],
    };

    errorMessage = "";

    installEvent = null;

    constructor(
        private formBuilder: FormBuilder,
        private navCtrl: NavController,
        private modalController: ModalController,
        private data: DataService,
        private swUpdate: SwUpdate
    ) {}

    ngOnInit() {
        this.updatePWA();
        this.loginForm = this.formBuilder.group({
            email: new FormControl(
                "",
                Validators.compose([
                    Validators.required,
                    Validators.pattern(
                        "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"
                    ),
                ])
            ),
            password: new FormControl(
                "",
                Validators.compose([
                    Validators.required,
                    Validators.minLength(6),
                ])
            ),
        });
    }

    loginUser(credentials) {
        this.navCtrl.navigateForward("/home");
        /*this.authService
      .signInWithEmailAndPassword(credentials.email, credentials.password)
      .subscribe(() => {
      });/** */
    }

    logout() {
        //this.authService.logout().subscribe(() => {});
    }

    authGoogle() {
        /*this.authService.signInWithGoogle().subscribe(() => {
      this.navCtrl.navigateForward("/menu/home");
    });/** */
    }

    sendPasswordResetEmail(email: string) {
        //this.authService.sendPasswordResetEmail(email).subscribe();
    }

    goToRegister() {
        this.navCtrl.navigateForward("/sign-up");
    }

    goToPasswordReset() {
        this.navCtrl.navigateForward("/reset-password");
    }

    async open_modal(b) {
        let modal;
        if (b) {
          modal = await this.modalController.create({
            component: InfoPage,
            componentProps: { value: this.data.terms_of_use, title: 'Términos y Condiciones' }
          });
        } else {
          modal = await this.modalController.create({
            component: InfoPage,
            componentProps: { value: this.data.privacy_policy, title: 'Políticas de Privacidad' }
          });
        }
        return await modal.present();
      }

      
    updatePWA() {
        this.swUpdate.available.subscribe((value) => {
            console.log("update", value);
            window.location.reload();
        });
    }

    @HostListener("window:beforeinstallprompt", ["$event"])
    onBeforeInstallPrompt(event: Event) {
        console.log('++++++++++++++++++1');
        console.log(event);
        event.preventDefault();
        console.log('++++++++++++++++++2');
        this.installEvent = event;
        console.log('++++++++++++++++++3');
    }

    installByUser() {
        console.log('--------------1');
        console.log(!!this.installEvent);
        if (this.installEvent) {
            this.installEvent.prompt();
            console.log('--------------2');
            this.installEvent.userChoice().then((rta) => {
                console.log('--------------3');
                console.log(rta);
            });
        }
    }
}
