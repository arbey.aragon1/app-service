import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MaintenanceOrderListPageRoutingModule } from './maintenance-order-list-routing.module';

import { MaintenanceOrderListPage } from './maintenance-order-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaintenanceOrderListPageRoutingModule
  ],
  declarations: [MaintenanceOrderListPage]
})
export class MaintenanceOrderListPageModule {}
