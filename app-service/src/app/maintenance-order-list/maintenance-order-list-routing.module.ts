import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MaintenanceOrderListPage } from './maintenance-order-list.page';

const routes: Routes = [
  {
    path: '',
    component: MaintenanceOrderListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MaintenanceOrderListPageRoutingModule {}
