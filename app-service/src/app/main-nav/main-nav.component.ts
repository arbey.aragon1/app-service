import { Component, OnInit } from "@angular/core";
import { MenuController } from "@ionic/angular";
import { NavController } from "@ionic/angular";

@Component({
    selector: "main-nav",
    templateUrl: "./main-nav.component.html",
    styleUrls: ["./main-nav.component.scss"],
})
export class MainNavComponent implements OnInit {

   
    public selectedIndex = 0;
    public appPages = [
        {
            title: "Servicios",
            url: this.navCtrl.navigateForward("/home"),
            icon: "construct-outline",
        },
        {
            title: "Ordenes",
            url: "/folder/Ordenes",
            icon: "reader-outline",
        },
        {
            title: "Chat",
            url: "/folder/Chat",
            icon: "chatbubble-ellipses-outline",
        },
        {
            title: "Soporte",
            url: "/folder/Soporte",
            icon: "help-circle-outline",
        },
    ];
    public labels = [
        "Servicios",
        "Ordenes",
        "Chat",
        "Soporte",
    ];

    constructor(private menu: MenuController,  private navCtrl: NavController,) {}

    ngOnInit() {}

    public buttonClick(index, label) {
        this.selectedIndex = index;
        console.log(label);
    }
  
}


