import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MaintenanceOrderDetailPage } from './maintenance-order-detail.page';

const routes: Routes = [
  {
    path: '',
    component: MaintenanceOrderDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MaintenanceOrderDetailPageRoutingModule {}
