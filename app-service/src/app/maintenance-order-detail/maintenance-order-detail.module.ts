import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MaintenanceOrderDetailPageRoutingModule } from './maintenance-order-detail-routing.module';

import { MaintenanceOrderDetailPage } from './maintenance-order-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaintenanceOrderDetailPageRoutingModule
  ],
  declarations: [MaintenanceOrderDetailPage]
})
export class MaintenanceOrderDetailPageModule {}
