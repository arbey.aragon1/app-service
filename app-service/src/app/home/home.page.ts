import { Component, OnInit } from "@angular/core";

@Component({
    selector: "app-home",
    templateUrl: "./home.page.html",
    styleUrls: ["./home.page.scss"],
})
export class HomePage implements OnInit {
    navigate = [
        {
            title: "App",
            url: "/apps",
            icon: "apps",
        },
        {
            title: "Book",
            url: "/book",
            icon: "book",
        },
        {
            title: "Paint",
            url: "/paint",
            icon: "brush",
        },
        {
            title: "Contacts",
            url: "/contacts",
            icon: "contacts",
        },
        {
            title: "Facebook",
            url: "/facebook.com",
            icon: "logo-facebook",
        },
    ];

    constructor() {}

    ngOnInit() {}
}
