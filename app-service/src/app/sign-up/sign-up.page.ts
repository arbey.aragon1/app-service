import { Component, DebugElement, OnInit } from "@angular/core";
import {
    AbstractControl,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from "@angular/forms";
import { NavController } from "@ionic/angular";
import { AuthService } from "../services/auth.service";
import { InfoPage } from "../info/info.page";
import { ModalController } from "@ionic/angular";
import { DataService } from "../services/data.service";

@Component({
    selector: "app-sign-up",
    templateUrl: "./sign-up.page.html",
    styleUrls: ["./sign-up.page.scss"],
})
export class SignUpPage implements OnInit {
    registerForm: FormGroup;

    validation_message = {
        name: [{ type: "required", message: "El Nombre es requerido" }],
        lastName: [{ type: "required", message: "El Apellido es requerido" }],
        /*
        companyName: [
            {
                type: "required",
                message: "El nombre de la empresa es requerido",
            },
        ],
        */
        phone: [
            { type: "required", message: "El teléfono móvil es requerido" },
        ],
        email: [
            { type: "required", message: "El email es requerido" },
            { type: "pattern", message: "Este no es un email válido" },
        ],
        password: [
            { type: "required", message: "El password es requerido" },
            { type: "minlength", message: "Minimo 6 letras para el password" },
        ],
    };

    errorMessage = "";
    constructor(
        private formBuilder: FormBuilder,
        private navCtrl: NavController,
        private auth: AuthService,
        private modalController: ModalController,
        private data: DataService
    ) {}

    ngOnInit() {
        this.registerForm = this.formBuilder.group(
            {
                name: new FormControl(
                    "",
                    Validators.compose([Validators.required])
                ),
                lastName: new FormControl(
                    "",
                    Validators.compose([Validators.required])
                ),
                /*
                companyName: new FormControl(
                    "",
                    Validators.compose([Validators.required])
                ),
                */
                phone: new FormControl(
                    "",
                    Validators.compose([Validators.required])
                ),
                email: new FormControl(
                    "",
                    Validators.compose([
                        Validators.required,
                        Validators.pattern(
                            "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"
                        ),
                    ])
                ),
                password: new FormControl(
                    "",
                    Validators.compose([
                        Validators.required,
                        Validators.minLength(6),
                    ])
                ),
                confirmPassword: new FormControl(
                    "",
                    Validators.compose([Validators.required])
                ),
            },
            { validator: this.passwordConfirming }
        );
    }

    passwordConfirming(c: AbstractControl): { invalid: boolean } {
        if (c.get("password").value !== c.get("confirmPassword").value) {
            return { invalid: true };
        }
    }

    registerUser(credentials) {
        console.log(credentials);

        this.auth
            .signInWithEmailAndPassword(
                credentials.name,
                credentials.lastName,
                credentials.phone,
                credentials.email,
                credentials.password
            )
            .subscribe(
                () => {
                    // this.navCtrl.navigateForward("/menu/home");
                    console.log("Auth");
                },
                () => {
                    console.log("Error");
                }
            );
    }

    goToBack() {
        this.navCtrl.navigateBack("/login");
    }

    goToLogin() {
        this.navCtrl.navigateForward("/login");
    }

    async open_modal(b) {
        let modal;
        if (b) {
            modal = await this.modalController.create({
                component: InfoPage,
                componentProps: {
                    value: this.data.terms_of_use,
                    title: "Términos y Condiciones",
                },
            });
        } else {
            modal = await this.modalController.create({
                component: InfoPage,
                componentProps: {
                    value: this.data.privacy_policy,
                    title: "Políticas de Privacidad",
                },
            });
        }
        return await modal.present();
    }
}
