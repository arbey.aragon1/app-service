import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TellUsYourNumberPage } from './tell-us-your-number.page';

describe('TellUsYourNumberPage', () => {
  let component: TellUsYourNumberPage;
  let fixture: ComponentFixture<TellUsYourNumberPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TellUsYourNumberPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TellUsYourNumberPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
