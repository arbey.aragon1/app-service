import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TellUsYourNumberPage } from './tell-us-your-number.page';

const routes: Routes = [
  {
    path: '',
    component: TellUsYourNumberPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TellUsYourNumberPageRoutingModule {}
