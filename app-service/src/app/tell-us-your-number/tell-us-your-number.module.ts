import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TellUsYourNumberPageRoutingModule } from './tell-us-your-number-routing.module';

import { TellUsYourNumberPage } from './tell-us-your-number.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TellUsYourNumberPageRoutingModule
  ],
  declarations: [TellUsYourNumberPage]
})
export class TellUsYourNumberPageModule {}
