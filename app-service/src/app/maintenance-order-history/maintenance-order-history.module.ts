import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MaintenanceOrderHistoryPageRoutingModule } from './maintenance-order-history-routing.module';

import { MaintenanceOrderHistoryPage } from './maintenance-order-history.page';

import { ComponentsModule } from "../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaintenanceOrderHistoryPageRoutingModule,
    ComponentsModule
  ],
  declarations: [MaintenanceOrderHistoryPage]
})
export class MaintenanceOrderHistoryPageModule {}
