import { Component, OnInit } from '@angular/core';
import { NavController } from "@ionic/angular";
@Component({
  selector: 'app-maintenance-order-history',
  templateUrl: './maintenance-order-history.page.html',
  styleUrls: ['./maintenance-order-history.page.scss'],
})
export class MaintenanceOrderHistoryPage implements OnInit {

  constructor(private navCtrl: NavController) { }

  ngOnInit() {
  }

  goToBack() {
    this.navCtrl.back();
}
}
