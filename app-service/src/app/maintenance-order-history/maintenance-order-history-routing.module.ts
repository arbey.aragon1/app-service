import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MaintenanceOrderHistoryPage } from './maintenance-order-history.page';

const routes: Routes = [
  {
    path: '',
    component: MaintenanceOrderHistoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MaintenanceOrderHistoryPageRoutingModule {}
