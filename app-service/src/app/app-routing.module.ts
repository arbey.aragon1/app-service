import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
    {
        path: "",
        redirectTo: "landing",
        pathMatch: "full",
    },
    {
        path: "landing",
        loadChildren: () =>
            import("./landing/landing.module").then((m) => m.LandingPageModule),
    },
    {
        path: "login",
        loadChildren: () =>
            import("./login/login.module").then((m) => m.LoginPageModule),
    },
    {
        path: "maintenance-order-detail",
        loadChildren: () =>
            import(
                "./maintenance-order-detail/maintenance-order-detail.module"
            ).then((m) => m.MaintenanceOrderDetailPageModule),
    },
    {
        path: "maintenance-order-list",
        loadChildren: () =>
            import(
                "./maintenance-order-list/maintenance-order-list.module"
            ).then((m) => m.MaintenanceOrderListPageModule),
    },
    {
        path: "maintenance-order-history",
        loadChildren: () =>
            import(
                "./maintenance-order-history/maintenance-order-history.module"
            ).then((m) => m.MaintenanceOrderHistoryPageModule),
    },
    {
        path: "account-settings",
        loadChildren: () =>
            import("./account-settings/account-settings.module").then(
                (m) => m.AccountSettingsPageModule
            ),
    },
    {
        path: "help",
        loadChildren: () =>
            import("./help/help.module").then((m) => m.HelpPageModule),
    },
    {
        path: "payment",
        loadChildren: () =>
            import("./payment/payment.module").then((m) => m.PaymentPageModule),
    },
    {
        path: "messeges",
        loadChildren: () =>
            import("./messeges/messeges.module").then(
                (m) => m.MessegesPageModule
            ),
    },
    {
        path: "certification",
        loadChildren: () =>
            import("./certification/certification.module").then(
                (m) => m.CertificationPageModule
            ),
    },
    {
        path: "sign-up",
        loadChildren: () =>
            import("./sign-up/sign-up.module").then((m) => m.SignUpPageModule),
    },
    {
        path: "reset-password",
        loadChildren: () =>
            import("./reset-password/reset-password.module").then(
                (m) => m.ResetPasswordPageModule
            ),
    },
    {
        path: "news",
        loadChildren: () =>
            import("./news/news.module").then((m) => m.NewsPageModule),
    },
    {
        path: "home",
        loadChildren: () =>
            import("./home/home.module").then((m) => m.HomePageModule),
    },
    {
        path: "notifications",
        loadChildren: () =>
            import("./notifications/notifications.module").then(
                (m) => m.NotificationsPageModule
            ),
    },
    {
        path: "my-account",
        loadChildren: () =>
            import("./my-account/my-account.module").then(
                (m) => m.MyAccountPageModule
            ),
    },
    {
        path: "tell-us-your-number",
        loadChildren: () =>
            import("./tell-us-your-number/tell-us-your-number.module").then(
                (m) => m.TellUsYourNumberPageModule
            ),
    },
    {
        path: "verification-sms",
        loadChildren: () =>
            import("./verification-sms/verification-sms.module").then(
                (m) => m.VerificationSmsPageModule
            ),
    },  {
    path: 'info',
    loadChildren: () => import('./info/info.module').then( m => m.InfoPageModule)
  },

];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
