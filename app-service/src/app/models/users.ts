export class User {
    constructor(
        private _uid: string,
        private _status: number,
        private _name: string,
        private _lastName: string,
        private _email: string,
        private _phone: string
    ) {}

    get uid(): string {
        return this._uid;
    }

    get status(): number {
        return this._status;
    }

    get name(): string {
        return this._name;
    }

    get lastName(): string {
        return this._lastName;
    }

    get email(): string {
        return this._email;
    }

    get phone(): string {
        return this._phone;
    }

    public static fromJSON({
        uid,
        status,
        name,
        lastName,
        email,
        phone,
    }): User {
        return new User(uid, status, name, lastName, email, phone);
    }

    public static fromJSONArray(array: any[]): User[] {
        return array
            .map((value: any) => {
                return { uid: value.key, ...value.payload.val() };
            })
            .map(User.fromJSON);
    }

    /**
     * README:
     * toJSON no utiliza el key del usuario
     * Se debe utilizar este método para guardar el usuario en
     * firebase con el método push
     */
    public toJSON(): any {
        return {
            status: this.status,
            name: this.name,
            lastName: this.lastName,
            email: this.email,
            phone: this.phone,
        };
    }
}
