export const environment = {
    production: true,
    firebase: {
        apiKey: "AIzaSyCGPnOdnDFZmnzVC9eIeNShIXSzZZD5lX0",
        authDomain: "app-services-1d27e.firebaseapp.com",
        databaseURL: "https://app-services-1d27e.firebaseio.com",
        projectId: "app-services-1d27e",
        storageBucket: "app-services-1d27e.appspot.com",
        messagingSenderId: "497033520090",
        appId: "1:497033520090:web:499f9d71b21e53450ad6e3",
        measurementId: "G-C9QR4YK5L5",
    },
};
