// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyCGPnOdnDFZmnzVC9eIeNShIXSzZZD5lX0",
        authDomain: "app-services-1d27e.firebaseapp.com",
        databaseURL: "https://app-services-1d27e.firebaseio.com",
        projectId: "app-services-1d27e",
        storageBucket: "app-services-1d27e.appspot.com",
        messagingSenderId: "497033520090",
        appId: "1:497033520090:web:499f9d71b21e53450ad6e3",
        measurementId: "G-C9QR4YK5L5",
    },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
