import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
//import { UserService } from "./userFunctions";

admin.initializeApp(functions.config().firebase);
//const userService = new UserService(admin, functions);

export const createUserMock = functions.https.onRequest((request, response) => {
    functions.logger.info("Hello logs!", { structuredData: true });
    //response.send("Hello from Firebase!");
});

export const createUser = functions.auth.user().onCreate((user) => {
    functions.logger.info("CreateUser!", { structuredData: true });
    //userService.createUser(user).subscribe(() => {});
});

export const deleteUser = functions.auth.user().onDelete((user) => {
    functions.logger.info("DeleteUser!", { structuredData: true });
    // userService.deleteUser(user);
});
