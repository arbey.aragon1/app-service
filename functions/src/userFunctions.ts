import { from } from "rxjs";
import { User } from "./user";

export class UserService {
    admin: any;
    functions: any;
    constructor(admin: any, functions: any) {
        this.admin = admin;
        this.functions = functions;
    }

    createUser(userData: any) {
        this.functions.logger.info("Create user!", { structuredData: true });
        this.functions.logger.info(userData, { structuredData: true });
        const user = new User("uid", 0, "name", "lastName", "email", "phone");
        return from(
            this.admin
                .database()
                .ref("/users/" + user.uid)
                .set(user.toJSON())
        );
    }

    deleteUser(userData: any) {
        this.functions.logger.info("Delete usar!", { structuredData: true });
        this.functions.logger.info(userData, { structuredData: true });
        return this.admin
            .database()
            .ref("/users/" + userData.uid)
            .delete();
    }
}
